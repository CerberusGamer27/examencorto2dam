import React from 'react';
import {
    NativeBaseProvider, VStack, ZStack, Stack, Center, Box, Heading,
    Divider, StyleSheet, Text, View, Button, ScrollView, FormControl
    , InputGroup, InputLeftAddon, WarningOutlineIcon, Input
} from 'native-base';
import * as yup from 'yup';
import { Formik } from 'formik';
import { useNavigation } from '@react-navigation/native';


export default function Index({ navigation }) {


    return (
        <NativeBaseProvider>
            <ScrollView>
                <Box mt={5} flex={1} p={1} w="95%" mx='auto' justifyContent={'center'}>
                    <VStack space={2} px="2" alignItems="center" bg={'amber.100'} rounded='50' justifyContent="center">
                        <Heading size="md" pt={'3'}>Menu Opciones</Heading>
                        <Stack mb="2.5" mt="1.5" direction={{
                            base: "column",
                            md: "row"
                        }} space={2} mx={{
                            base: "auto",
                            md: "0"
                        }}>
                            <Button size="lg" variant="outline" onPress={() => navigation.navigate('InformacionPersonal')}>
                                Informacion Personal
                            </Button>
                            <Button size="lg" variant="outline" onPress={() => navigation.navigate('ReactNative')}>
                                React Native
                            </Button>
                            <Button size="lg" variant="outline" onPress={() => navigation.navigate('Php')}>
                                PHP
                            </Button>
                            <Button size="lg" variant="outline" onPress={() => navigation.navigate('Angular')}>
                                Angular
                            </Button>
                        </Stack>
                    </VStack>
                </Box>
            </ScrollView>
        </NativeBaseProvider>
    )
}

