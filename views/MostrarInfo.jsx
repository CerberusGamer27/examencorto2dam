import { BackHandler, ScrollView } from 'react-native';
import {
    NativeBaseProvider, Text, VStack, Heading, Box, HStack, Avatar,
    WarningOutlineIcon
} from 'native-base';
import { useEffect } from 'react';


const imagenFondo = { uri: "https://img.wallpapersafari.com/phone/1080/1920/0/16/2EvYKa.jpg" }

export default function MostrarInfo({ navigation, route }) {
    
    useEffect(() => {
        const backAction = () => {
            navigation.navigate('Index');
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
    
        return () => backHandler.remove();
    }, []);

    const { nombre1, nombre2, carnet1, carnet2 } = route.params;

    return (
        <NativeBaseProvider>
            {/* <ImageBackground source={imagenFondo} resizeMode="cover" style={{ flex: 1, justifyContent: "center" }}> */}
            <ScrollView >
                <Box mt={5} flex={1} p={1} w="95%" mx='auto'>
                    <HStack justifyContent="space-between" space={2} alignItems={"center"}>
                        <Heading style={styles.tituloForm} size="sm">Datos Empleado</Heading>
                        <Avatar source={{ uri: "https://img1.ak.crunchyroll.com/i/spire4/be7ccab083087be99884531cadd7d5651630065450_large.png" }} size="xl" justifyContent="center">
                            <Avatar.Badge bg={"green.500"} />
                        </Avatar>
                    </HStack>
                    <VStack space={8} backgroundColor='blue.100' pt={'5'} pb={'5'} rounded={'25'} alignItems="flex-start">
                        <Text paddingLeft={'10'}><Text bold>Nombre Alumno 1 </Text>: {nombre1}</Text>
                        <Text paddingLeft={'10'}><Text bold>Nombre Alumno 2 </Text>: {nombre2}</Text>
                        <Text paddingLeft={'10'}><Text bold>Carnet Alumno 1 </Text>: {carnet1}</Text>
                        <Text paddingLeft={'10'}><Text bold>Carnet Alunno 2</Text>: {carnet1}</Text>

                    </VStack>
                </Box>
            </ScrollView>
            {/* </ImageBackground> */}
        </NativeBaseProvider>
    );
}

const styles = {
    inputSeleccionado: {
        bg: "coolGray.200:alpha.100"
    },
    botonDisabled: {
        backgroundColor: '#00aeef'
    },
    labelInput: {
        color: 'white',
        fontSize: 'sm',
        fontWeight: 'bold'
    },
    tituloForm: {
        color: '#8796FF'
    }
}