import { React } from "react";
import { Box, AspectRatio, Center, Stack, Heading, Text, HStack, useTheme, NativeBaseProvider, Image } from "native-base";

const Php = () => {
    return (
        <NativeBaseProvider>
            <Box alignItems="center">
                <Box maxW="80" rounded="lg" overflow="hidden" paddingTop={1} borderColor="coolGray.200" borderWidth="1" _dark={{
                    borderColor: "coolGray.600",
                    backgroundColor: "gray.700"
                }} _web={{
                    shadow: 2,
                    borderWidth: 0
                }} _light={{
                    backgroundColor: "gray.50"
                }}>
                    <Box>
                        <AspectRatio w="95%" ratio={18 / 9}>
                            <Image source={{
                                uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1920px-PHP-logo.svg.png"
                            }} alt="image" />
                        </AspectRatio>
                    </Box>
                    <Stack p="4" space={3}>
                        <Stack space={2}>
                            <Heading size="md" ml="-1">
                                PHP
                            </Heading>
                            <Text fontSize="xs" _light={{
                                color: "violet.500"
                            }} _dark={{
                                color: "violet.400"
                            }} fontWeight="500" ml="-0.5" mt="-1">
                                Lenguaje de Programación
                            </Text>
                        </Stack>
                        <Text fontWeight="400">
                        Fue creado inicialmente por el programador danés-canadiense Rasmus Lerdorf en 1994. En la actualidad, 
                        la implementación de referencia de PHP es producida por The PHP Group PHP originalmente significaba Personal Home Page (Página personal), 
                        pero ahora significa el inicialismo recursivo PHP: Hypertext Preprocessor
                        </Text>
                    </Stack>
                </Box>
            </Box>
        </NativeBaseProvider>
    )
};

export default Php;