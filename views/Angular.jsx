import { React } from "react";
import { Box, AspectRatio, Center, Stack, Heading, Text, HStack, useTheme, NativeBaseProvider, Image } from "native-base";

const Angular = () => {
    return (
        <NativeBaseProvider>
            <Box alignItems="center">
                <Box maxW="80" rounded="lg" overflow="hidden" paddingTop={1} borderColor="coolGray.200" borderWidth="1" _dark={{
                    borderColor: "coolGray.600",
                    backgroundColor: "gray.700"
                }} _web={{
                    shadow: 2,
                    borderWidth: 0
                }} _light={{
                    backgroundColor: "gray.50"
                }}>
                    <Box>
                        <AspectRatio w="95%" ratio={18 / 9}>
                            <Image source={{
                                uri: "https://sg.com.mx/sites/default/files/styles/570x500/public/images/angular-logo.png?itok=_4hR0cNu"
                            }} alt="image" />
                        </AspectRatio>
                    </Box>
                    <Stack p="4" space={3}>
                        <Stack space={2}>
                            <Heading size="md" ml="-1">
                                Angular
                            </Heading>
                            <Text fontSize="xs" _light={{
                                color: "violet.500"
                            }} _dark={{
                                color: "violet.400"
                            }} fontWeight="500" ml="-0.5" mt="-1">
                                Framework para aplicaciones web
                            </Text>
                        </Stack>
                        <Text fontWeight="400">
                        Angular (comúnmente llamado Angular 2+ o Angular 2) es un framework para aplicaciones web desarrollado en TypeScript, 
                        de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página. 
                        Su objetivo es aumentar las aplicaciones basadas en navegador con capacidad de Modelo Vista Controlador (MVC), 
                        en un esfuerzo para hacer que el desarrollo y las pruebas sean más fáciles.
                        </Text>
                    </Stack>
                </Box>
            </Box>
        </NativeBaseProvider>
    )
};

export default Angular