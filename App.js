import React from "react";
import { Button } from "react-native"

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Index from "./views/Index";
import MostrarInfo from "./views/MostrarInfo";
import ReactNative from "./views/ReactNative";
import Php from "./views/Php";
import Angular from "./views/Angular";
import InformacionPersonal from "./views/InformacionPersonal";

import {HeaderBackButton} from "@react-navigation/elements"

const Stack = createNativeStackNavigator();



export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Index" component={Index} options={{ title: "Menu Principal" }} />
        <Stack.Screen name="MostrarInfo" component={MostrarInfo} options={({ navigation }) => ({
          title: 'Mostrar Info',
          headerLeft: () => (
            <HeaderBackButton onPress={() => navigation.navigate('Index')} />
          ),
        })} />

        <Stack.Screen name="ReactNative" component={ReactNative} options={{ title: "React Native" }} />
        <Stack.Screen name="Php" component={Php} options={{ title: "PHP" }} />
        <Stack.Screen name="Angular" component={Angular} options={{ title: "Angular" }} />
        <Stack.Screen name="InformacionPersonal" component={InformacionPersonal} options={{ title: "Informacion Personal" }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}